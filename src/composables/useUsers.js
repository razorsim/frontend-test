import { usersApiUrl, postHeaders } from "../services";
import { api_token } from "@/services";
import { ref } from "vue";

export const useUsers = () => {
  const users = ref([]);
  const usersLoading = ref(false);
  const usersError = ref(false);
  const fetchUsers = async () => {
    try {
      usersLoading.value = true;
      const res = await fetch(usersApiUrl("lists"));
      users.value = await res.json();
    } catch (error) {
      usersError.value = error;
    } finally {
      usersLoading.value = false
      usersLoading.value = false;
    }
  };

  const friendRequestLoading = ref(false);
  const friendRequestError = ref(false);
  /**
   *
   * @param {{records: [fields: {From: string, To: string[]}]}} data
   */
  const sendFriendRequest = async (data) => {
    try {
      friendRequestLoading.value = true;
      const res = await fetch(usersApiUrl("friendRequest"), {
        method: "POST",
        headers: postHeaders,
        body: JSON.stringify(data),
      });
      return res
    } catch (error) {
      friendRequestLoading.value = false;
      friendRequestError.value = error;
      return false
    } finally {
      friendRequestLoading.value = false;
    }
  };

  const user = ref({});
  const userLoading = ref(false);
  const userError = ref(false);
  /**
   * 
   * @param {string} id 
   */
  const fetchUser = async (id) => {
    try {
      userLoading.value = true;
      const res = await fetch(`${usersApiUrl("show")}${id}?api_key=${api_token}`);
      user.value = await res.json();
    } catch (error) {
      userLoading.value = false;
      userError.value = error;
    } finally {
      userLoading.value = false;
    }
  };

  return {
    users,
    usersLoading,
    usersError,
    fetchUsers,
    friendRequestLoading,
    friendRequestError,
    sendFriendRequest,
    user,
    userLoading,
    userError,
    fetchUser
  };
};
