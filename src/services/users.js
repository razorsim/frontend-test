
export const postHeaders = {
  Authorization: `Bearer ${process.env.VUE_APP_API_TOKEN}`,
  "Content-Type": "application/json",
};
export const usersApiUrl = (model) => {
  switch (model) {
    case "lists":
      return `https://api.airtable.com/v0/app7cLoZ4xMOrQvKR/People?api_key=${process.env.VUE_APP_API_TOKEN}`;
    case "show":
      return "https://api.airtable.com/v0/app7cLoZ4xMOrQvKR/People/";
    case "friendRequest":
      return "https://api.airtable.com/v0/app7cLoZ4xMOrQvKR/Friend%20request";
    default:
      return "";
  }
};
