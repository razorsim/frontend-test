import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";

import App from "./App.vue";
import Home from "./views/Home.vue";
import User from "./views/User.vue";
import "./assets/tailwind.css";
import "./assets/fonts.css";


const routes = [
  {
    path: "/",
    component: Home,
  },
  {
    path: "/user/:id",
    component: User,
  },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
})
const app = createApp(App);
app.use(router)
app.mount('#app')
