module.exports = {
  mode: 'jit',
  purge: ['./public/**/*.html',
  './src/**/*.{js,jsx,ts,tsx,vue}',],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        beige: {
          500: "#E0DBD0",
          100: "#F6F4EF",
        },
        green: {
          500: "#8DCC7B",
        }
      },
      fontFamily: {
        sans: ["Work Sans", "sans-serif"],
      }
    },
  },
  variants: {
    extend: {},
  },
};
